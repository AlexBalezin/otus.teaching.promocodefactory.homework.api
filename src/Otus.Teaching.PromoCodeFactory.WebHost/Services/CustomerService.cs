﻿using Grpc.Core;
using GrpcCustomer;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Threading.Tasks;
using System;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Castle.Core.Resource;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomerService : GrpcCustomer.Customer.CustomerBase
    {
        private readonly IRepository<Core.Domain.PromoCodeManagement.Customer> _customerRepository;
        private readonly IRepository<Core.Domain.PromoCodeManagement.Preference> _preferenceRepository;

        public CustomerService(IRepository<Core.Domain.PromoCodeManagement.Customer> customerRepository,
            IRepository<Core.Domain.PromoCodeManagement.Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<GetCustomersResponse> GetCustomers(GetCustomersRequest request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = new GetCustomersResponse
            {
                GetCustomerResponses =
                {
                    customers.Select(x => new GetCustomerResponse
                    {
                        Id = x.Id.ToString("N"),
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Email = x.Email,
                        Preferences =
                        {
                            x.Preferences.Select(y => new GrpcCustomer.Preference
                            {
                                Id = y.PreferenceId.ToString("N"),
                                Name = y.Preference.Name
                            }).ToList()
                        }
                    }).ToList()
                }
            };

            return response;
        }

        public override async Task<GetCustomerResponse> GetCustomer(GetCustomerRequest request, ServerCallContext context)
        {
            Guid idCustomer;

            if (Guid.TryParse(request.Id, out idCustomer))
            {
                throw new Exception("Customer id is unvalid");
            }

            var customer = await _customerRepository.GetByIdAsync(idCustomer);

            var response = new GetCustomerResponse
            {
                Id = customer.Id.ToString("N"),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences =
                {
                    customer.Preferences.Select(x => new GrpcCustomer.Preference
                    {
                        Id = x.PreferenceId.ToString("N"),
                        Name = x.Preference.Name
                    }).ToList()
                }
            };
            return response;
        }

        public override async Task<CreateCustomerResponse> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(p => Guid.Parse(p)).ToList());

            Core.Domain.PromoCodeManagement.Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return new CreateCustomerResponse { Id = customer.Id.ToString("N") };
        }

        public override async Task<EditCustomerResponse> EditCustomer(EditCustomerRequest request, ServerCallContext context)
        {
            Guid idCustomer;

            if (Guid.TryParse(request.Id, out idCustomer))
            {
                throw new Exception("Customer id is unvalid");
            }

            var customer = await _customerRepository.GetByIdAsync(idCustomer);

            if (customer == null)
            {
                throw new Exception("Not found");
            }

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(
                request.Preferences.Select(x => Guid.Parse(x.Id)).ToList());

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);
            return new EditCustomerResponse();
        }

        public override async Task<DeleteCustomerResponse> DeleteCustomer(DeleteCustomerRequest request, ServerCallContext context)
        {
            Guid idCustomer;

            if (Guid.TryParse(request.Id, out idCustomer))
            {
                throw new Exception("Customer id is unvalid");
            }

            var customer = await _customerRepository.GetByIdAsync(idCustomer);

            if (customer == null)
            {
                throw new Exception("Not found");
            }

            await _customerRepository.DeleteAsync(customer);

            return new DeleteCustomerResponse();
        }


    }
}
